export class LoginPage {
    loginTo(email, password) {

        // Visit the website
        // cy.visit('/')

        // Type Username and verify
        cy.get('[placeholder="Email address"]').should('exist').type(email).wait(1000)
        // .invoke('prop', 'value').then(Username => {
        //     expect(Username).to.eql(email)
        // })

        // Type Password and verify
        cy.get('[placeholder="Password"]').should('exist').type(password).wait(1000)
        // .invoke('prop', 'value').then(Password => {
        //     expect(Password).to.eql(password)
        // })

        // Check default state of the password field (should be masked)
        cy.get('[placeholder="Password"]').should('have.attr', 'type', 'password').wait(1000)

        // Click the mask button to reveal the password
        cy.get('[class="iconify iconify--ri"]').should('exist').click().wait(1000)
        cy.get('[placeholder="Password"]').should('have.attr', 'type', 'text')

        // Check the state of the mask button and toggle if needed
        cy.get('[class="iconify iconify--lucide"]').then(button => {
            if (button.length) {
                // If the password is visible, click the mask button to hide the password
                cy.get('[class="iconify iconify--lucide"]').should('exist').click().wait(1000)
                cy.get('[placeholder="Password"]').should('have.attr', 'type', 'password')
            }
        })

        // Click Login Button
        cy.get('.login-btn').click().wait(2000)
    }
}

export const OnLoginPage = new LoginPage()