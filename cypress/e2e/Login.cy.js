import { OnLoginPage } from "../support/PageObjects/LoginPage/Login"

describe("login", () => {

    beforeEach("Visit this URL", () => {
        //Visit the base url of the website
        cy.visit('/')
    })

    it("TC-01: Verify Login with valid email & password", () => {
        OnLoginPage.loginTo('sexaxa5018@wuzak.com', 'Maverick@07')
        cy.get('.profile-img > :nth-child(1)').click().wait(1000)
        cy.get('.user-setting > .button-1 > .btn-text').click().wait(1000)
    })

    it("TC-02: Verify Login with invalid email & valid password", () => {
        OnLoginPage.loginTo('sexaxa501wuzak.com', 'Maverick@07')
    })

    it("TC-03: Verify Login with valid email & invalid password", () => {
        OnLoginPage.loginTo('sexaxa5018@wuzak.com', 'Maverick@0')
    })

    it("TC-04: Verify Login with invalid email & password", () => {
        OnLoginPage.loginTo('sexaxa5018wuzak.com', 'Maveric@0')
    })

    it("TC-05: Verify Login with unregistered email", () => {
        OnLoginPage.loginTo('kabirwiit18@gmail.com', 'Maverick@07')

        cy.on('window:alert', (txt) => {
            expect(txt).to.equal('Invalid Credential')  //Assertion
        })
        cy.log()
    })

    it.only("TC-06: Verify Login with empty email & password field", () => {
        OnLoginPage.loginTo(" ", " ")
    })

})
