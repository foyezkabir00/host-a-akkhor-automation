describe("login", () => {

    it("login", () => {

        // Visit the website
        cy.visit('/')

        // Type Username and verify
        cy.get('[placeholder="Email address"]').should('exist').type('sexaxa5018@wuzak.com').wait(1000)
            .invoke('prop', 'value').then(Username => {
                expect(Username).to.eql('sexaxa5018@wuzak.com')
            });

        // Type Password and verify
        cy.get('[placeholder="Password"]').should('exist').type('Maverick@07').wait(1000)
            .invoke('prop', 'value').then(Password => {
                expect(Password).to.eql('Maverick@07')
            });

        // Toggle the state of the password mask
        cy.get('[placeholder="Password"]').then($passwordField => {
            const isPasswordMasked = $passwordField.attr('type') === 'password';

            // Click the mask button
            cy.get('[class="iconify iconify--ri"]').click().wait(1500);

            // Verify the changed state
            if (isPasswordMasked) {
                // If it was masked, now it should be visible
                cy.get('[placeholder="Password"]').should('have.attr', 'type', 'text');
            } else {
                // If it was visible, now it should be masked
                cy.get('[placeholder="Password"]').should('have.attr', 'type', 'password');
            }
        });

        // Click Login Button
        cy.get('.login-btn').click().wait(2000);
    });

    afterEach('LogOut', () => {
        cy.get('.profile-img > :nth-child(1)').click().wait(1000);
        cy.get('.user-setting > .button-1 > .btn-text').click().wait(1500);
    });
});
