const { defineConfig } = require('cypress')

module.exports = defineConfig({
  viewportHeight: 1080,
  viewportWidth: 1920,
  video: false,
  e2e: {
    baseUrl: 'https://qa.d1im1pd1tn0tbq.amplifyapp.com/',
    testIsolation: false,
    setupNodeEvents(on, config) {
      // implement node event listeners here
    }
  }
})
